-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2021 at 09:46 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dtt`
--

-- --------------------------------------------------------

--
-- Table structure for table `house`
--

CREATE TABLE `house` (
  `id` int(11) NOT NULL,
  `street` varchar(50) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `addition` varchar(50) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `bedroomCount` int(11) DEFAULT NULL,
  `livingroomCount` int(11) DEFAULT NULL,
  `bathroomCount` int(11) DEFAULT NULL,
  `toiletCount` int(11) DEFAULT NULL,
  `storageCount` int(11) DEFAULT NULL,
  `createdByUserId` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `house`
--

INSERT INTO `house` (`id`, `street`, `number`, `addition`, `zipcode`, `city`, `bedroomCount`, `livingroomCount`, `bathroomCount`, `toiletCount`, `storageCount`, `createdByUserId`) VALUES
(1, 'Eagan', 89, 'k', '9055-DZ', 'Gangou', 3, 2, 3, 3, 2, 1),
(2, 'Jenifer', 1, NULL, '8336-SV', 'Bojongsari', 1, 3, 3, 1, 2, 1),
(3, 'Maryland', 58, 'a', '9788-WC', 'Tayabamba', 1, 3, 1, 2, 2, 1),
(4, 'Fuller', 48, 'm', '5465-TJ', 'Ulricehamn', 3, 1, 2, 1, 3, 1),
(5, 'Clyde Gallagher', 22, 'q', '5813-QO', 'Burgersdorp', 1, 3, 3, 3, 1, 1),
(6, 'Bobwhite', 30, 'p', '9362-YR', 'Kadusirung Hilir', 3, 1, 2, 2, 2, 1),
(7, 'Elmside', 30, 'k', '5860-QI', 'Ketanggi', 2, 1, 1, 3, 2, 1),
(8, 'Anthes', 110, 'h', '8540-IS', 'Sanban', 3, 2, 2, 1, 1, 1),
(9, 'Lake View', 12, 'u', '6277-SR', 'Yichun', 2, 3, 2, 1, 1, 1),
(10, 'Thierer', 135, 'y', '1428-MK', 'Gugarkâ€™', 1, 2, 2, 1, 1, 1),
(11, 'Magdeline', 120, 'n', '6968-PG', 'Jardin', 3, 2, 2, 1, 1, 1),
(12, 'Pepper Wood', 21, 'c', '0192-QZ', 'Claremorris', 1, 1, 2, 2, 3, 1),
(13, 'Lakeland', 69, 'j', '6630-MY', 'Saint-AndrÃ©-Avellin', 3, 1, 2, 3, 1, 1),
(14, 'Cascade', 38, 'a', '5128-CB', 'Sambirejo', 3, 1, 1, 3, 3, 1),
(15, 'Lien', 108, 'u', '2834-CD', 'Noebana', 1, 3, 1, 2, 3, 1),
(16, 'Sunfield', 49, 'x', '6631-QK', 'Melfi', 2, 1, 1, 3, 1, 1),
(17, 'Twin Pines', 69, 'q', '6249-FZ', 'Freguesia do Ribeirao da Ilha', 2, 2, 3, 2, 1, 1),
(18, 'Warrior', 98, 'e', '2891-ZT', 'Socabaya', 3, 2, 3, 1, 3, 1),
(19, 'Calypso', 128, 'u', '6897-UI', 'Brzeszcze', 3, 3, 1, 2, 2, 1),
(20, 'Colorado', 145, 'u', '5231-KN', 'Asahbadung', 1, 2, 1, 3, 2, 1),
(21, '8th', 30, 'q', '9091-RG', 'Chunoyar', 2, 2, 2, 1, 2, 1),
(22, 'Everett', 111, 'j', '7153-ZN', 'Wakapuken', 1, 1, 2, 3, 3, 1),
(23, 'Eliot', 68, NULL, '6914-YC', 'Yongtai', 2, 1, 3, 2, 2, 1),
(24, 'Fairfield', 43, 'y', '6548-UW', 'Pirojpur', 2, 3, 1, 3, 2, 1),
(25, 'American Ash', 56, NULL, '8149-WJ', 'Wenlin', 2, 3, 1, 3, 1, 1),
(26, 'Knutson', 114, 'e', '4462-RY', 'San Juan Nepomuceno', 3, 2, 2, 1, 2, 1),
(27, 'Butternut', 94, 'g', '7319-VS', 'Reckange-sur-Mess', 2, 2, 3, 3, 3, 1),
(28, 'Melby', 53, 'f', '5273-JF', 'Coja', 2, 1, 1, 2, 3, 1),
(29, 'Anhalt', 5, 'd', '6445-JG', 'Pantai', 3, 1, 3, 3, 1, 1),
(30, 'Nobel', 130, NULL, '2911-XD', 'BraganÃ§a', 2, 2, 2, 2, 3, 1),
(31, 'Pierstorff', 27, 'k', '4146-TN', 'YaguarÃ³n', 3, 1, 2, 1, 3, 1),
(32, 'Mayfield', 8, 'v', '7171-LN', 'JÃ³zefÃ³w', 3, 1, 3, 1, 1, 1),
(33, 'Vera', 130, 'u', '8129-LS', 'GloÅ¾an', 1, 2, 1, 1, 2, 1),
(34, 'Village Green', 4, 'h', '1707-VP', 'Minamata', 2, 2, 3, 2, 1, 1),
(35, 'Algoma', 49, 'z', '2435-SY', 'KallonÃ­', 2, 1, 2, 2, 1, 1),
(36, 'Pine View', 23, 'j', '8400-AT', 'Bel Air RiviÃ¨re SÃ¨che', 2, 3, 3, 2, 1, 1),
(37, 'Stuart', 22, 'b', '5992-CS', 'VÃ¤xjÃ¶', 3, 1, 1, 2, 3, 1),
(38, 'Susan', 66, 'g', '9504-NY', 'Haikou', 3, 2, 3, 1, 1, 1),
(39, 'Forster', 58, 'a', '6113-DF', 'Wuhu', 3, 1, 1, 3, 2, 1),
(40, 'Helena', 108, 'r', '1279-AU', 'Xinzhou', 1, 1, 3, 2, 2, 1),
(41, 'Lawn', 43, 'e', '7284-VP', 'Cut-cut Primero', 1, 1, 3, 3, 1, 1),
(42, 'Warrior', 93, 'k', '7315-EG', 'Rawa', 1, 3, 2, 2, 2, 1),
(43, 'Scofield', 19, 'z', '2856-XT', 'FontaÃ­nhas', 3, 2, 3, 2, 1, 1),
(44, 'Caliangt', 86, 'l', '9672-RK', 'Oslo', 3, 3, 2, 3, 1, 1),
(45, 'Rigney', 14, 'd', '7412-AR', 'Kizlyar', 3, 3, 3, 1, 3, 1),
(46, 'Northridge', 110, 'v', '2340-JD', 'Nanterre', 1, 3, 2, 1, 3, 1),
(47, 'Sommers', 31, 'v', '7906-SE', 'Shenjing', 1, 3, 3, 1, 1, 1),
(48, 'Debra', 103, 'n', '2710-KJ', 'Viana', 2, 1, 2, 2, 2, 1),
(49, 'Atwood', 83, 't', '4960-KS', 'Gunungangka', 2, 3, 2, 1, 2, 1),
(50, 'Derek', 14, 'b', '7109-RC', 'Araripina', 1, 3, 3, 1, 1, 1),
(51, 'Artisan', 68, 'h', '7060-KN', 'Qiaogu', 1, 1, 3, 2, 3, 1),
(52, 'Thackeray', 52, 'v', '8827-FA', 'Oliveira', 1, 2, 1, 3, 3, 1),
(53, 'Victoria', 56, 'y', '7308-CP', 'Lanigan', 3, 3, 3, 3, 1, 1),
(54, 'Rieder', 70, 'a', '3948-JI', 'Jilin', 3, 1, 1, 3, 2, 1),
(55, 'Jana', 100, 'o', '1391-SI', 'MieÅ›cisko', 1, 2, 1, 2, 3, 1),
(56, 'Fremont', 146, 'a', '4168-CO', 'Dingqiao', 2, 2, 1, 3, 3, 1),
(57, 'Kinsman', 11, 'u', '5185-LJ', 'Kihancha', 2, 2, 2, 2, 2, 1),
(58, 'Oak', 33, 'm', '6807-JL', 'Le Havre', 2, 2, 3, 3, 3, 1),
(59, 'Sloan', 41, 'p', '1616-LI', 'Mirocin', 3, 3, 1, 1, 3, 1),
(60, 'Tennyson', 120, 'w', '4541-SH', 'Detusoko', 3, 1, 1, 1, 2, 1),
(61, 'Katie', 143, 'w', '8833-VT', 'DÄ™bno', 1, 1, 2, 1, 2, 1),
(62, 'Merchant', 84, 'w', '0616-QW', 'KelÄ«shÄd va SÅ«darjÄn', 3, 3, 1, 3, 3, 1),
(63, 'Manufacturers', 51, 'l', '0440-OY', 'Eci', 1, 2, 1, 2, 2, 1),
(64, 'Monica', 95, 'h', '0226-ZT', 'Tulsa', 2, 2, 1, 2, 2, 1),
(65, 'Debra', 68, 'm', '3606-EY', 'Al Wudayâ€˜', 1, 3, 3, 3, 2, 1),
(66, 'Pine View', 137, 'n', '9291-KJ', 'Thá»‹ Tráº¥n Na Hang', 3, 3, 2, 1, 1, 1),
(67, 'International', 103, 'r', '6856-ZB', 'Plessisville', 1, 2, 2, 1, 1, 1),
(68, 'Pankratz', 83, NULL, '0393-IS', 'Pasarkuok', 3, 1, 2, 2, 2, 1),
(69, 'Express', 71, 'b', '5788-PW', 'Shuiji', 3, 2, 2, 1, 2, 1),
(70, 'Onsgard', 141, 'p', '0809-CD', 'Grujugan', 2, 3, 2, 2, 1, 1),
(71, 'Dovetail', 120, 'm', '8611-HO', 'Erdenet', 1, 2, 2, 1, 3, 1),
(72, 'Paget', 11, 'p', '7572-WH', 'Santa Teresita', 2, 2, 1, 3, 2, 1),
(73, 'Dunning', 36, 'b', '4984-QW', 'QuilpuÃ©', 3, 3, 2, 2, 1, 1),
(74, 'Kinsman', 94, 'd', '0671-LD', 'Privodino', 3, 3, 1, 1, 3, 1),
(75, 'Northland', 149, 'c', '8024-FD', 'Borda da Mata', 3, 2, 3, 3, 1, 1),
(76, 'Namekagon', 124, 'j', '9398-HX', 'Jingzi', 3, 2, 3, 2, 1, 1),
(77, 'Donald', 28, 'b', '0758-EN', 'Bamako', 3, 2, 1, 1, 1, 1),
(78, 'Nova', 36, 'f', '2480-GT', 'Lukhovka', 3, 2, 2, 3, 1, 1),
(79, 'Nobel', 32, 'r', '2693-BP', 'Yingchengzi', 3, 1, 2, 3, 2, 1),
(80, 'Marcy', 81, 'j', '7947-UD', 'Xiaolin', 1, 3, 2, 2, 3, 1),
(81, 'Arkansas', 3, 'z', '0576-PB', 'New Orleans', 1, 1, 1, 3, 1, 1),
(82, 'Butternut', 147, 't', '9322-QO', 'Honglu', 1, 3, 1, 2, 3, 1),
(83, 'East', 75, 'w', '9769-VT', 'Courtaboeuf', 3, 1, 3, 2, 2, 1),
(84, 'Harper', 90, NULL, '6787-RY', 'Yizhivtsi', 3, 3, 3, 3, 3, 1),
(85, 'Portage', 53, NULL, '3451-UT', 'NisÃ­', 1, 3, 3, 3, 2, 1),
(86, 'American Ash', 75, 'z', '1691-DB', 'DaitÅchÅ', 3, 1, 2, 2, 1, 1),
(87, 'Haas', 2, 'i', '5731-UP', 'Naikoten Dua', 3, 2, 3, 2, 3, 1),
(88, 'Hovde', 80, 'x', '9961-JR', 'Hengli', 3, 2, 2, 3, 2, 1),
(89, 'Tony', 121, 'p', '2201-XS', 'Fale old settlement', 1, 2, 1, 1, 2, 1),
(90, 'Jana', 110, 'r', '2195-PU', 'Riversdale', 1, 1, 2, 3, 1, 1),
(91, 'Daystar', 115, 'm', '8845-ZD', 'Jiuxian', 1, 2, 3, 1, 3, 1),
(92, 'Declaration', 67, 'o', '7394-RN', 'Dubreuil', 3, 1, 3, 3, 3, 1),
(93, 'Vera', 52, 'a', '6569-OJ', 'Lebak', 2, 1, 3, 2, 1, 1),
(94, 'Melody', 67, 'k', '2767-GI', 'Itapetininga', 2, 2, 1, 1, 3, 1),
(95, 'Bellgrove', 131, 'j', '8218-SR', 'Le Mans', 3, 1, 3, 1, 2, 1),
(96, 'Holy Cross', 21, 'p', '6084-RH', 'Rio de Janeiro', 1, 1, 1, 3, 3, 1),
(97, 'Lyons', 40, 'm', '1811-NN', 'ThaÌ£nh HoÌa', 2, 2, 3, 3, 2, 1),
(98, 'Saint Paul', 148, 'b', '8020-GK', 'Binjai', 1, 2, 3, 3, 1, 1),
(99, 'Dryden', 60, 'c', '0236-NO', 'Gorzyce', 1, 3, 1, 3, 2, 1),
(100, 'Sugar', 69, 'l', '4995-TX', 'Mondorf-les-Bains', 2, 2, 2, 3, 3, 1),
(101, 'Dakota', 140, 'x', '8767-MI', 'Ordino', 2, 2, 3, 2, 2, 1),
(102, 'Hazelcrest', 58, 'x', '8943-OP', 'ManolÃ¡s', 3, 1, 2, 3, 2, 1),
(103, 'Anniversary', 131, 'z', '5536-XM', 'Monte Carmelo', 3, 3, 2, 3, 3, 1),
(104, 'Gina', 59, 'f', '8913-WD', 'Rubizhne', 2, 3, 2, 1, 1, 1),
(105, 'Corben', 110, 'g', '0855-KL', 'Ricardo Flores Magon', 3, 3, 1, 2, 2, 1),
(106, 'Tomscot', 40, 'b', '5401-PI', 'Coromandel', 2, 1, 1, 3, 3, 1),
(107, 'Judy', 91, 'd', '0882-FJ', 'Nanto-shi', 3, 2, 3, 2, 3, 1),
(108, 'Amoth', 80, 'a', '3314-FT', 'Tuodian', 1, 2, 2, 3, 3, 1),
(109, 'Hazelcrest', 43, 'u', '6097-CJ', 'Los Arcos', 3, 1, 3, 1, 2, 1),
(110, 'Brentwood', 112, 's', '8399-PZ', 'VÅ™esina', 2, 3, 2, 1, 3, 1),
(111, 'Linden', 124, NULL, '8116-NL', 'Chon Daen', 2, 3, 3, 1, 2, 1),
(112, 'Blaine', 53, 'a', '5498-BV', 'Ketangi', 3, 1, 3, 1, 1, 1),
(113, 'Shopko', 16, 'a', '5347-CU', 'Xinpeicun', 3, 2, 2, 1, 2, 1),
(114, 'Kennedy', 53, 'u', '4786-CV', 'Miguel Calmon', 3, 3, 2, 3, 3, 1),
(115, 'Spaight', 72, NULL, '4385-SP', 'Frydrychowice', 1, 3, 3, 1, 1, 1),
(116, 'Manufacturers', 25, 'x', '1824-FC', 'Khao Kho', 1, 3, 1, 3, 1, 1),
(117, 'Calypso', 72, 'p', '5636-ZT', 'Notre-Dame-des-Prairies', 2, 2, 2, 3, 3, 1),
(118, 'Armistice', 91, 'r', '1886-LR', 'Maguan', 3, 3, 2, 1, 2, 1),
(119, 'Grim', 23, 'k', '8360-NY', 'Minas de Matahambre', 1, 1, 1, 3, 3, 1),
(120, 'Del Sol', 123, 't', '6013-JY', 'Coronel', 2, 3, 2, 1, 3, 1),
(121, 'Lillian', 128, 'a', '5408-ST', 'Magapit', 2, 3, 2, 2, 2, 1),
(122, 'Anthes', 32, 'r', '3728-SV', 'PortariÃ¡', 2, 1, 3, 3, 2, 1),
(123, 'Dayton', 51, NULL, '0131-HK', 'Castleknock', 2, 1, 3, 1, 2, 1),
(124, 'Maywood', 34, NULL, '3992-FE', 'JÃ¶nkÃ¶ping', 2, 2, 3, 2, 3, 1),
(125, 'Leroy', 54, 'e', '4888-FQ', 'Cennan', 2, 1, 2, 3, 1, 1),
(126, 'Coleman', 100, 'p', '6924-YU', 'San Pedro Necta', 3, 1, 1, 1, 1, 1),
(127, 'Lukken', 29, 'q', '6713-UM', 'Machico', 1, 2, 1, 1, 1, 1),
(128, 'Thompson', 74, 'h', '0579-VM', 'Novosilâ€™', 2, 3, 2, 3, 2, 1),
(129, 'Moland', 14, 's', '4336-QE', 'La Habana Vieja', 2, 2, 2, 1, 2, 1),
(130, 'Lukken', 103, 'w', '9445-YD', 'Jedlina-ZdrÃ³j', 2, 3, 1, 2, 2, 1),
(131, 'Mandrake', 6, 'n', '6042-LO', 'Changkeng', 2, 3, 3, 3, 3, 1),
(132, 'Boyd', 108, 'v', '0207-EX', 'Sindangrasa', 3, 3, 3, 1, 1, 1),
(133, 'Victoria', 122, 't', '5728-AC', 'Mulhouse', 1, 2, 3, 1, 2, 1),
(134, 'Northridge', 51, NULL, '5801-EP', 'Ayaviri', 3, 3, 3, 2, 1, 1),
(135, 'Heffernan', 76, 'w', '2564-MG', 'QuintÃ£s', 3, 1, 3, 3, 2, 1),
(136, 'Redwing', 106, 'f', '4255-CC', 'Qiganjidie', 2, 2, 2, 3, 2, 1),
(137, 'Paget', 73, 'z', '2798-ZM', 'Cova da Iria', 2, 3, 2, 2, 3, 1),
(138, 'Barnett', 72, 't', '7522-LX', 'SabunÃ§u', 1, 2, 2, 1, 3, 1),
(139, 'Westerfield', 25, 'r', '4414-MS', 'Pantin', 1, 3, 2, 2, 2, 1),
(140, 'Clyde Gallagher', 121, 'u', '1979-MI', 'Baruchowo', 1, 3, 3, 2, 3, 1),
(141, 'Clove', 121, 'a', '0013-WY', 'Sanqiao', 2, 2, 3, 3, 1, 1),
(142, 'Hollow Ridge', 44, 'b', '5654-QX', 'Mantang', 3, 3, 1, 3, 3, 1),
(143, 'Boyd', 79, 'g', '8978-AL', 'Proletar', 1, 2, 3, 3, 3, 1),
(144, 'Dovetail', 136, 't', '3787-UV', 'Sidomukti', 2, 2, 1, 3, 3, 1),
(145, 'Redwing', 144, 'y', '3361-TI', 'KÃ¶ping', 3, 1, 2, 3, 3, 1),
(146, 'Del Mar', 63, 'b', '8514-YI', 'PÃ¤rnu-Jaagupi', 3, 2, 1, 2, 2, 1),
(147, 'Bultman', 79, 'b', '9963-JN', 'Sijunjung', 3, 2, 1, 1, 1, 1),
(148, 'Mcguire', 23, 'c', '5008-PQ', 'Atiquizaya', 1, 1, 2, 2, 2, 1),
(149, 'Maple Wood', 69, 'c', '3682-EM', 'Duzhou', 2, 3, 3, 2, 1, 1),
(150, 'Lighthouse Bay', 44, 'l', '2913-PZ', 'Larangan', 2, 1, 1, 3, 3, 1),
(151, 'test', 1, 'test', 'test', 'test', 1, 1, 1, 1, 1, 2),
(152, 'test', 1, 'test', 'test', 'test', 1, 1, 1, 1, 1, 2),
(153, 'ingenhouszstraat', 53, '', '3514hv', 'UTRECHT', 3, 1, 1, 2, 2, 3),
(304, '53', 123, '123', '3514hv', 'UTRECHT', 1, 1, 1, 1, 0, 1),
(306, 'test', 1233, 'test', 'test', 'test', 2, 11, 1, 3, 3, 5),
(307, '53', 123, '123', '3514hv', 'UTRECHT', 3, 2, 1, 2, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `room` varchar(30) NOT NULL,
  `width` int(4) NOT NULL,
  `height` int(4) NOT NULL,
  `length` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `room`, `width`, `height`, `length`) VALUES
(1, 'living room', 1250, 2560, 2500),
(2, 'living room', 1250, 2560, 2500),
(3, 'bedroom', 1250, 2560, 2500),
(4, 'bathroom', 1250, 2560, 2500),
(5, 'toilet', 1250, 2560, 2500),
(6, 'storage', 1250, 2560, 2500);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `surname` varchar(30) DEFAULT NULL,
  `emailAddress` varchar(70) DEFAULT NULL,
  `role` varchar(30) DEFAULT '2',
  `validationkey` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `firstname`, `surname`, `emailAddress`, `role`, `validationkey`, `status`, `createdat`, `updatedat`) VALUES
(3, 'test3', '$2y$10$R0pPVUVKdmZJMXlXNFErTeNw161oGZ6djLZEEGdGaHP1Do3TrKljq', 'test3', 'test3', 'test3@gmail.com', '1', '9b34651e083b566406da14976476adab', 'Active', '2021-07-11 16:27:01', NULL),
(4, 'afrokid', '$2y$10$N2NGaXBvN0dXVnlUNDFzd.xBWXjW7S07VHvW.C4ZCftS97GEvr0Yy', 'max', 'van Gorp', 'maxvangorp1001@gmail.com', '2', '8e42842dda6636d7d4823ef3699f013f', 'Active', '2021-07-12 14:16:01', NULL),
(5, 'test ', '$2y$10$bkJYWDdyWVRjSXlsM1RaZOiTmhdb9PWrsX6mrx6CZT9W/1ThM1XbO', 'test', 'test', 'test@test.com', 'Registered User', '29ee6f0487247f8df163c77e097f5a77', 'Active', '2021-07-12 15:29:22', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `house`
--
ALTER TABLE `house`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `house`
--
ALTER TABLE `house`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
